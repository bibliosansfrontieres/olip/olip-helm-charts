# olip-helm-charts

A Helm Chart to deploy OLIP to Kubernetes.

## The Helm Chart

* `chart.yaml`
* `values.yaml` : Variables utilisées par le dossier template
* `templates/`
  * `ingress.yaml`
    [doc](https://kubernetes.io/fr/docs/concepts/services-networking/ingress/)
    Among the available managed ingress controlers, we chose Traefik V2.
  * `service.yaml`
    [doc](https://kubernetes.io/docs/concepts/services-networking/service/)
    Trafic from Ingress flows to a Service. It listens on a specific port for
    incoming connections,and routes the traffic to a Pod as specified in a
    Deployment file.
  * `deployment.yaml`
    [doc](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/)
    Runs a pod, sets its replicas amount, the Docker image, some mount-points
    for persistent storage, etc.
  * `configmap.yaml`
    [doc](https://kubernetes.io/docs/concepts/configuration/configmap/)
    Handles configuration values (`key=value`) or raw data which will be
    mounted into a Pod. Useful for declaring scripts or YAML configuration files.
  * `pv-claim.yaml`
    [doc](https://kubernetes.io/docs/tasks/configure-pod-container/configure-persistent-volume-storage/#create-a-persistentvolumeclaim)
    Allows to claim for a storage, eventually specifying its size or its usage
    policy (data deletion on termination, etc).

## Deploy a new instance

```shell
cd charts/
kubectl create namespace MY_PROJECT_NAME
helm upgrade --install --namespace="MY_PROJECT_NAME" --set domainName=example.com MY_PROJECT_NAME .
```

### Set a Domain Name

Obviously, the domain name used in a deployment must point to the
loadbalancer public IP address - as found in the
[Scaleway management console](https://console.scaleway.com/load-balancer/ips):

```shell
--set domainName=example.com
```

### Set storage space

Read `values.yaml` to override values from command line.

You can define persistent storage for your instance, here with 100Gb of disk space:

```shell
--set persistentStorage.enabled="true" --set persistentStorage.olipStorageSize="100Gi"
```

#### Upsize a Volume

An instance is linked to a PV (Physical Volume), a SCW Block Storage.
The instance uses this PV through a PVC (Physical Volume Claim).
This PVC references the disk storage space allocatd to the instance.

* Browse to the [instance's Volumes Manager](https://console.scaleway.com/instance/volumes),
  then rise its size
* Edit the PVC: `kubectl edit pvc` and rise its size to the fine value
* kill the Pod! `kubectl delete pod coallia-api-deployment-f7c74d547-9nn57`

A new Pod is created. Check the new allocated storage size:

```shell
kubectl exec coallia-api-deployment-f7c74d547-rjjxq -c api -- df -h
```

### Populate OLIP with an existing DB

```shell
--set olipDB="https://s3.eu-central-1.wasabisys.com/olip-db/idc-fra-kit-sante/olip/d876c5121b3f4d05a74c7a9646fa9572/srv.tar"
```

## Reach the instance

* OLIP Dashboard : `http://MY_PROJECT_NAME.example.com`
* OLIP API : `http://api.MY_PROJECT_NAME.example.com`
* Mediacenter : `http://mediacenter.MY_PROJECT_NAME.example.com`
* Statistics : `http://stats.MY_PROJECT_NAME.example.com`
  (only available when `persistentStorage.enabled="true"`)
* etc.

## Destroy the instance

```shell
helm delete MY_PROJECT_NAME
kubectl delete namespaces MY_PROJECT_NAME
```

## Technical details

We use a Traefik2 Scaleway loadbalancer deployed following [this documentation](https://www.scaleway.com/en/docs/tutorials/traefik-v2-cert-manager/?facetFilters=%5B%22categories%3Akubernetes%22%5D&page=1#creating-a-service-to-deploy-a-loadbalancer-in-front-of-traefik-2):

The `traefik-loadbalancer.yml` file:

```yaml
apiVersion: v1
kind: Service
metadata:
  name: traefik-ingress
  namespace: kube-system
  labels:
    k8s.scw.cloud/ingress: traefik2
spec:
  type: LoadBalancer
  ports:
    - port: 80
      name: http
      targetPort: 8000
    - port: 443
      name: https
      targetPort: 8443
  selector:
    app.kubernetes.io/name: traefik
```

```shell
kubectl create -f traefik-loadbalancer.yml
```

---

To enable access log for Traefik Ingress,
apply the `traefik-patch.yml` file:

```shell
kubectl -n kube-system patch daemonset traefik --type merge --patch "$(cat traefik-patch.yml)"
```
