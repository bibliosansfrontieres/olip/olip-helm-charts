---
apiVersion: v1
kind: ConfigMap
metadata:
  name: traefik-config
data:
  traefik.yml: |
    entryPoints:
      web:
        address: ":80"
      websecure:
        address: ":443"
    providers:
      docker: {}
    api:
      insecure: true
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: docker-config-script
data:
  docker-config-script.sh: |
    #!/bin/sh
    
    docker_network_id=$(docker network inspect olip --format {{ printf "{{.Id}}" }} 2>/dev/null)
    
    if [ -z $docker_network_id ]
      then docker network create olip
    fi
    
    if [ -z $(docker images -q --filter=reference='traefik') ]
      then docker run -d -p 8008:80 --name traefik --network olip --restart always -v /mnt/traefik/:/etc/traefik/ -v /var/run/docker.sock:/var/run/docker.sock traefik:v2.5.4
    elif ! $(docker container inspect --format {{ printf "{{.State.Running}}" }} traefik 2>/dev/null)
      then docker start traefik
    fi
{{- if .Values.persistentStorage.enabled }}
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: goaccess-report-builder
data:
  report-builder.sh: |
    #!/bin/sh

    # The current files needs the following params
    TITLE='OLIP dashboard'

    CUSTOM_CSS='static/report.css'
    CUSTOM_JS='static/report.js'
    OUTPUT='index.html'
    
    # call goaccess with common switches
    run_goaccess() {
      goaccess - \
        --html-report-title="$TITLE" \
        --html-custom-css="$CUSTOM_CSS" \
        --html-custom-js="$CUSTOM_JS" \
        --ignore-panel=GEO_LOCATION --ignore-panel=REFERRING_SITES \
        --ignore-panel=REQUESTS_STATIC --ignore-panel=OS --ignore-panel=REFERRERS \
        --ignore-panel=STATUS_CODES --ignore-panel=REMOTE_USER \
        --ignore-panel=BROWSERS \
        -q --no-query-string --ignore-crawlers \
        --hour-spec=min --date-spec=hr \
        -o "/var/www/html/$OUTPUT"
    }
    
    # cats the log files
    cat_logs() {
      # current logfile
      cat /logs/access.log
      # rotated logfiles - logrotate is configured to use dateext
      cat /logs/access.log-20??????
      # rotated logfiles - compressed
      zcat /logs/access.log*.gz
    }
    
    cat_logs \
      | awk '!/\.js|\.css|api\@docker|api\@internal|stats|oidc|silent-renew|api-version/' \
      | sed -e 's/@docker//' -e 's/@internal//' \
      | run_goaccess
    
    for report_type in day week month
    do 
        OUTPUT="report_$report_type.html"
        TIME=$report_type

        cat_logs \
          | sed -n '/'$(date '+%d\/%b\/%Y' -d '1 '$TIME' ago')'/,$ p' \
          | awk '!/\.js|\.css|api\@docker|api\@internal|stats|oidc|silent-renew|api-version/' \
          | sed -e 's/@docker//' -e 's/@internal//' \
          | run_goaccess
    done

---
apiVersion: v1
kind: ConfigMap
metadata:
  name: logparser
data:
  logparser.py: |
    # -*- coding: utf-8 -*-

    import json
    import datetime
    import os
    import sys

    class accesslogParser():
        
        def __init__(self):
            self.full_logfile=os.getenv('LOG_PATH')
            self.currated_logfile='/logs/access.log'
            self.domain_to_look_for = '{{ .Values.domainName }}'
            self.date = datetime.datetime.now().strftime("%Y%m%d")

        def parse_accesslog(self):
            print(f'Using {self.full_logfile}')

            with open(self.full_logfile, 'r') as f:
                
                file_to_write_log = open(self.currated_logfile + "-" + self.date, "w")
            
                while True:
                    raw_line = f.readline()
                    
                    if not raw_line:
                        break
                        
                    if "request_Referer" in raw_line:
                        
                        line = json.loads(''.join(raw_line.split()[3:]))

                        if self.domain_to_look_for in line["RequestHost"]:

                            date_from_iso = datetime.datetime.strptime(line["time"], "%Y-%m-%dT%H:%M:%SZ")
                            date = date_from_iso.strftime("%d/%b/%Y:%H:%M:%S")

                            clf_log_line = line["ClientHost"] + " - " + "- [" + date + " +0000] \"" + line["RequestMethod"] + " " + line["RequestPath"] + " " + line["RequestProtocol"] + "\" " + str(line["OriginStatus"]) + " " + str(line["OriginContentSize"]) + " \"" + line["request_Referer"] + "\" \"" + line["request_User-Agent"] + "\" - \"" + line["RequestHost"] + "\" - " + str(line["Duration"]) + "ms\n"

                            file_to_write_log.writelines(clf_log_line)

    if __name__ == "__main__":
      parse = accesslogParser()
      parse.parse_accesslog()
    
    sys.exit()

---
apiVersion: v1
kind: ConfigMap
metadata:
  name: goaccess-config
data:
  goaccess.conf: |
    time-format %T
    date-format %d/%b/%Y
    log-format %h %^ %e [%d:%t %^] "%r" %s %b "%R" "%u" %^ "%v" %^ %Lms
    config-dialog false
    hl-header true
    json-pretty-print false
    no-color false
    no-column-names false
    no-csv-summary false
    no-progress false
    no-tab-scroll false
    with-mouse false
    agent-list false
    with-output-resolver false
    http-method yes
    http-protocol yes
    no-query-string false
    no-term-resolver false
    444-as-404 false
    4xx-to-unique-count false
    all-static-files false
    double-decode false
    ignore-crawlers false
    crawlers-only false
    ignore-panel REFERRERS
    ignore-panel KEYPHRASES
    real-os true
    static-file .css
    static-file .js
    static-file .jpg
    static-file .png
    static-file .gif
    static-file .ico
    static-file .jpeg
    static-file .pdf
    static-file .csv
    static-file .mpeg
    static-file .mpg
    static-file .swf
    static-file .woff
    static-file .woff2
    static-file .xls
    static-file .xlsx
    static-file .doc
    static-file .docx
    static-file .ppt
    static-file .pptx
    static-file .txt
    static-file .zip
    static-file .ogg
    static-file .mp3
    static-file .mp4
    static-file .exe
    static-file .iso
    static-file .gz
    static-file .rar
    static-file .svg
    static-file .bmp
    static-file .tar
    static-file .tgz
    static-file .tiff
    static-file .tif
    static-file .ttf
    static-file .flv

---
apiVersion: v1
kind: ConfigMap
metadata:
  name: nginx-config
data:
  default.conf: |
    server {
        listen       80;
        server_name  localhost;

        location / {
            auth_basic "OLIP dashboard - Administrator's Area";
            auth_basic_user_file /usr/share/nginx/html/.htpasswd;
            root   /usr/share/nginx/html/;
            index  index.html;
        }
        
        # 404 error
        error_page 404 /404.html;
        location = /404.html {
          root /usr/share/nginx/html/;
          internal;
        }
    }
{{- end }}